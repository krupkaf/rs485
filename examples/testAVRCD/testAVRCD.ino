#include <RS485.h>

void setup() {
        Serial.begin(115200);
        RS485_1_Init(9600);
        for(uint8_t i=0; i<200; i++) {
                RS485_1_buff[i] = i;
        }

}

void loop() {
        uint8_t c;
        c = RS485_1_Receive();
        RS485_1_Transmit(c);
        Serial.print(F("Received bytes: "));
        Serial.print(c);
        Serial.print(F(" : "));
        for (uint8_t i = 0; i < c; i++) {
                Serial.print((char)RS485_1_buff[i]);
                Serial.print(F(", "));
        }
        Serial.println();
        delay(5000);
}
