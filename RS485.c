
#include "RS485.h"
#include <avr/interrupt.h>

#ifdef RS485_1_EN
uint8_t RS485_1_buff[RS485_1_BUFFSIZE];
uint8_t RS485_1_buffIndex = 0;
uint8_t RS485_1_buffEnd = 0;

SIGNAL(USART1_RX_vect) {
  RS485_1_buff[RS485_1_buffIndex] = UDR1;
  if (RS485_1_buffIndex < (RS485_1_BUFFSIZE - 1)) {
    RS485_1_buffIndex++;
  }
}

SIGNAL(USART1_TX_vect) { RS485_1_RWC; }

SIGNAL(USART1_UDRE_vect) {
  if (RS485_1_buffIndex < RS485_1_buffEnd) {
    UDR1 = RS485_1_buff[RS485_1_buffIndex];
    RS485_1_buffIndex++;
  } else {
    RS485_1_buffIndex = 0;
    UCSR1B &= ~_BV(UDRIE1);
  };
}

void RS485_1_Transmit(uint8_t packetSize) {
  if (packetSize == 0) {
    return;
  }
  RS485_1_buffEnd = packetSize;
  RS485_1_buffIndex = 0;
  RS485_1_RWS;
  UCSR1B |= (1 << UDRIE1);
}

uint8_t RS485_1_Receive(void) { return RS485_1_buffIndex; }

#endif
