/*
 * File:   RS485.h
 * Author: krupkaf
 */

#ifndef RS485_H
#define RS485_H

#include <ModBusMaster.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef RS485_1_EN
#ifndef RS485_1_BUFFSIZE
#define RS485_1_BUFFSIZE 256
#endif

extern uint8_t RS485_1_buff[RS485_1_BUFFSIZE];

#if defined(AVRCD324) || defined(AVRCD644) || defined(AVRCD1284)
#define RS485_1_RWC                                                            \
  { PORTD &= ~_BV(PD4); }
#define RS485_1_RWS                                                            \
  { PORTD |= _BV(PD4); }

#define RS485_1_Init(baudRate)                                                 \
  {                                                                            \
    UBRR1 = F_CPU / (baudRate * 16UL) - 1;                                     \
    DDRD |= _BV(PD4) | _BV(PD3);                                               \
    PORTD |= _BV(PD2);                                                         \
    RS485_1_RWC;                                                               \
    UCSR1A = 0x00;                                                             \
    UCSR1B = _BV(RXCIE1) | _BV(TXCIE1) | _BV(RXEN1) | _BV(TXEN1);              \
    UCSR0C = _BV(UCSZ11) | _BV(UCSZ10);                                        \
  }
#endif

void RS485_1_Transmit(uint8_t packetSize);
uint8_t RS485_1_Receive(void);

#endif

#ifdef __cplusplus
}
#endif

#endif /* RS485_H */
