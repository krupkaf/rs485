/*
 * File:   ModBudMaster.h
 * Author: krupkaf
 */

#ifndef RSMODBUSMASTER_H
#define RSMODBUSMASTER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Chybna delka paketu.
#define MB_E_PACKET_LENGTH -1
// Nesouhlasi CRC prisleho paketu.
#define MB_E_CRC -2
// Odpoved neodpovida pozadavku. Jina adresa.
#define MB_E_RESPONSE_ADDR -2
// Odpoved neodpovida pozadavku. Jina funkce.
#define MB_E_RESPONSE_CMD -2

// ModBus functions
// ReadInputRegister 0x04
void modbusReadInputRegisterQuery(uint8_t slaveAddr, uint16_t regAddr,
                                  uint8_t regCount);
uint8_t modbusReadInputRegisterResponse(uint8_t slaveAddr, uint8_t regCount,
                                        uint16_t *buff);

#ifdef __cplusplus
}
#endif

#endif /* RSMODBUSMASTER_H */
