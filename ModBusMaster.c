#include "RS485.h"
#include <crc.h>

// ModBus functions
void modbusSend(uint8_t PDUSize) {
  PDUSize++; //+prostor pro adresu
  *((uint16_t *)&RS485_1_buff[PDUSize]) = (crc16block(RS485_1_buff, PDUSize));
  PDUSize += 2; //+prostor pro crc.
  RS485_1_Transmit(PDUSize);
}

/**
 * Zakladni zpracovani prichoziho paketu.
 *
 * @param  PDUSize    Velikosp prisle PDU
 * @return            0 - Pokud vse v poradku, jinak kod chyby.
 */
uint8_t modbusReceive(uint8_t slaveAddr, uint8_t cmd, uint8_t *PDUSize) {
  uint8_t s = RS485_1_Receive();
  if (s == 0) {
    // Evidentne nic neprislo, neno co resit.
    return MB_E_PACKET_LENGTH;
  }
  if (crc16block(RS485_1_buff, s) != 0) {
    return MB_E_CRC; // Nesouhlasi CRC
  }
  if (RS485_1_buff[0] != slaveAddr) {
    return MB_E_RESPONSE_ADDR; // Prijata odpoved je od nekoho jineho, nez bylo
                               // ocekavano.
  }
  if ((RS485_1_buff[1] & 0x7f) != cmd) {
    // Odpoved je na jiny prikaz
    return MB_E_RESPONSE_CMD;
  }
  if ((RS485_1_buff[1] & 0x80) == 0x80) {
    // Je vracen chybovy kod
    return RS485_1_buff[2];
  }
  *PDUSize = s - 3;
  return 0;
}

void modbusReadInputRegisterQuery(uint8_t slaveAddr, uint16_t regAddr,
                                  uint8_t regCount) {
  RS485_1_buff[0] = slaveAddr;
  RS485_1_buff[1] = 4; // CMD
  *((uint16_t *)&RS485_1_buff[2]) = __builtin_bswap16(regAddr);
  *((uint16_t *)&RS485_1_buff[4]) = __builtin_bswap16(regCount);
  modbusSend(5);
}

uint8_t modbusReadInputRegisterResponse(uint8_t slaveAddr, uint8_t regCount,
                                        uint16_t *buff) {
  uint8_t PDUSize;
  uint8_t r = modbusReceive(slaveAddr, 4, &PDUSize);
  if (r != 0) {
    return r;
  }
  if (PDUSize != (regCount * 2 + 2)) {
    return PDUSize;
  };
  if (RS485_1_buff[2] != (2 * regCount)) {
    // Nesouhlasi delka odpovedi
    return -14;
  }

  uint16_t *bufferPtr = (uint16_t *)(RS485_1_buff + 3);
  for (; regCount; regCount--) {
    *buff = __builtin_bswap16(*bufferPtr);
    bufferPtr++;
    buff++;
  }
  return 0;
}
